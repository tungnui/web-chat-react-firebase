import React, { Component } from "react";

import Header from '../Header/Header';
import backgroundImg from '../../assets/img/header-background-2.jpg'

class LandingPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userName: ""
    };
  }

  onChange(e) {
    this.setState({ userName: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.state.userName) {
      this.props.handleSubmit(this.state.userName);
      return;
    }
    alert("Please choose your username first");
  }

  render() {
    const { userName } = this.state;

    return (
      <div id="landing-page">
        <div className="top-block">
          <div className="background-block">
            <img className="background-image" src={backgroundImg} alt="landing page background" />
            <div className="gray-layer" />
          </div>
          <Header />
          <form onSubmit={this.handleSubmit.bind(this)} className="login-form">
            <div className="login-input">
              <label htmlFor="login-field">Choose an user name</label>
              <input
                onChange={this.onChange.bind(this)}
                value={userName}
                id="login"
              />
            </div>
            <button type="submit">Login</button>
          </form>
        </div>
      </div>
    );
  }
}

export default LandingPage;
