import React from 'react'
import logo from '../../assets/svg/logo.svg'

const Header = () => (
  <div className="header-wrapper">
    <div className="header">
      <div className="left-block flex-row">
        <img src={logo} alt="logo" className="header-logo" />
        <div className="personal-info">
          <p className="name">Tung Pham</p>
          <p className="title">Front-End Developer</p>
        </div>
      </div>
      <div className="right-block">
        <button className="nav-button active">Home</button>
        <button className="nav-button">Projects</button>
      </div>
    </div>
  </div>
);

export default Header;
