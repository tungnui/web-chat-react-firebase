import React, { Component } from 'react';
import './assets/sass/screen.scss';

import LandingPage from './modules/LandingPage/LandingPage'
import ChatRoom from './modules/ChatRoom/ChatRoom'

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasLoggedIn: false
    }
  }

  handleSubmit(userName) {
    this.setState({ hasLoggedIn: true })
  }

  render() {
    const { hasLoggedIn } = this.state;

    return (
      <div>
        {hasLoggedIn ?
          <ChatRoom /> :
          <LandingPage
            handleSubmit={this.handleSubmit.bind(this)}
          />
        }
      </div>
    );
  }
}

export default App;
