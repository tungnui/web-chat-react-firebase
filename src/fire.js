import firebase from 'firebase'
import Rebase from 're-base'

var config = {
  apiKey: "AIzaSyB84t2heA1MSIc6u85pYGHQN7y8u2uv8iw",
  authDomain: "self-project-simple-web-chat.firebaseapp.com",
  databaseURL: "https://self-project-simple-web-chat.firebaseio.com",
  projectId: "self-project-simple-web-chat",
  storageBucket: "self-project-simple-web-chat.appspot.com",
  messagingSenderId: "226769077210"
};
var app = firebase.initializeApp(config);
var fire = Rebase.createClass(app.database())

export default fire;